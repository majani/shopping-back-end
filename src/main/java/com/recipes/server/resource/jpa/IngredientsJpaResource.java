package com.recipes.server.resource.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.recipes.server.models.Ingredient;
import com.recipes.server.repository.IngredientRepository;
import com.recipes.server.service.IngredientsService;

@Service
@Primary
public class IngredientsJpaResource implements IngredientsService {

	@Autowired
	IngredientRepository repo;

	@Override
	public Ingredient getIngredient(int id) {
		return repo.findById(id).get();
	}

	public List<Ingredient> findByRecipeId(int id) {
		return repo.findByRecipeId(id);
	}

	@Override
	public List<Ingredient> getIngredients() {
		return repo.findByOrderByIdAsc();
	}

	@Override
	public void addIngredient(Ingredient ingredient) {

		repo.save(ingredient);
	}

	@Override
	public void updateIngredient(Ingredient ingredient, int id) {
		repo.save(ingredient);
	}

	@Override
	public void deleteIngredient(int id) {

		repo.deleteById(id);
	}

}
