package com.recipes.server.resource.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.recipes.server.models.User;
import com.recipes.server.repository.UserRepository;

@Service
@Primary
public class UserJpaService implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(username);

		if (user != null) {
			return user;
		}

		throw new UsernameNotFoundException(username + "Was not found in the system");
	}

}
