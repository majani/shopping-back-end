package com.recipes.server.resource.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.recipes.server.models.User;
import com.recipes.server.repository.UserRepository;
import com.recipes.server.service.UserService;

@Service
@Primary
public class UserJpaResource implements UserService {

	@Autowired
	UserRepository userRepo;

	@Override
	public User getUser(int id) {
		return userRepo.findById(id).get();
	}

	@Override
	public List<User> getUsers() {
		return userRepo.findAll();
	}

	@Override
	public void addUser(User user) {
		userRepo.save(user);
	}

	@Override
	public void updateUser(User user, int id) {
		userRepo.save(user);
	}

	@Override
	public void deleteUser(int id) {
		userRepo.deleteById(id);
	}

}
