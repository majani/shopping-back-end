package com.recipes.server.resource.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.recipes.server.models.Ingredient;
import com.recipes.server.models.Recipe;
import com.recipes.server.repository.IngredientRepository;
import com.recipes.server.repository.RecipeRepository;
import com.recipes.server.service.RecipesService;

@Service
@Transactional
@Primary
public class RecipeJpaResource implements RecipesService {

	@Autowired
	RecipeRepository repo;

	@Autowired
	IngredientRepository ingRepo;

	@Override
	public Recipe getRecipe(int id) {
		Recipe recipe = repo.findById(id).get();
		List<Ingredient> recipeingredients = ingRepo.findByRecipeId(id);
		recipe.setIngredients(recipeingredients);
		return recipe;
	}

	@Override
	public List<Recipe> getRecipes() {
		List<Recipe> recipes = repo.findAll();

		for (Recipe recipe : recipes) {
			int id = recipe.getId();
			List<Ingredient> recipeIngredients = ingRepo.findByRecipeId(id);
			recipe.setIngredients(recipeIngredients);
		}

		return recipes;
	}

	@Override
	public void addRecipe(Recipe recipe) {

		// List<Ingredient> ingredients = recipe.getIngredients();

		repo.save(recipe);
		for (Ingredient ingredient : recipe.getIngredients()) {
			ingredient.setRecipe(recipe);
			ingRepo.save(ingredient);

		}
	}

	@Override
	public void updateRecipe(Recipe recipe, int id) {
		repo.save(recipe);
	}

	@Override
	public void deleteRecipe(int id) {

		ingRepo.deleteByRecipeId(id);
		repo.deleteById(id);

	}

}
