package com.recipes.server.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.recipes.server.models.Ingredient;
import com.recipes.server.models.Recipe;
import com.recipes.server.service.RecipesService;

@Service
//@Primary
public class RecipeResource implements RecipesService {

	private static List<Recipe> recipes = new ArrayList<Recipe>();
	private static List<Ingredient> ingredients = new ArrayList<>();
	private static int counter = 0;

	static {
		recipes.add(new Recipe(++counter, "Lahmacun", "Tadina bak",
				"https://www.takeaway.com/foodwiki/uploads/sites/11/2019/08/lahmacun_3-1080x964.jpg", ingredients));
		recipes.add(new Recipe(++counter, "Kiymali Pide", "En lezzetli pide",
				"https://i1.wp.com/www.gastrosenses.com/wp-content/uploads/2014/12/pide-with-lamb-1024x680.jpg?resize=1024%2C680&quality=100&strip=all",
				ingredients));
		recipes.add(new Recipe(++counter, "Adana kebab", "Hot and spicy",
				"https://turkishfoodie.com/wp-content/uploads/2018/11/Adana-Kebab-.jpg", ingredients));
		recipes.add(new Recipe(++counter, "Simit", "Sicak Simit",
				"https://i.ytimg.com/vi/XlQRM1hS0tk/maxresdefault.jpg", ingredients));
		recipes.add(new Recipe(++counter, "Kaiseri Mantisi", "Hot and spicy",
				"https://recipes.timesofindia.com/recipes/turkish-manti-dumpling/photo/59969178.cms", ingredients));

	}

	@Override
	public Recipe getRecipe(int id) {
		Recipe recipe = recipes.stream().filter(x -> x.getId() == id).findFirst().get();
		return recipe;

	}

	@Override
	public List<Recipe> getRecipes() {
		return recipes;
	}

	@Override
	public void addRecipe(Recipe recipe) {
		recipes.add(recipe);

	}

	@Override
	public void deleteRecipe(int id) {

		recipes.removeIf(x -> x.getId() == id);
	}

	@Override
	public void updateRecipe(Recipe recipe, int id) {
		// TODO Auto-generated method stub
		
	}

}
