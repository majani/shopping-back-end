package com.recipes.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IoRecipesServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IoRecipesServerApplication.class, args);
	}

}
