package com.recipes.server.service;

import java.util.List;

import com.recipes.server.models.User;

public interface UserService {
	public User getUser(int id);

	public List<User> getUsers();

	public void addUser(User user);
	
	public void updateUser(User user, int id);

	public void deleteUser(int id);
	

}
