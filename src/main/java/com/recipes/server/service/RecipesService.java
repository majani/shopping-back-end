package com.recipes.server.service;

import java.util.List;

import com.recipes.server.models.Recipe;

public interface RecipesService {

	public Recipe getRecipe(int id);

	public List<Recipe> getRecipes();

	public void addRecipe(Recipe recipe);
	
	public void updateRecipe(Recipe recipe, int id);

	public void deleteRecipe(int id);

}
