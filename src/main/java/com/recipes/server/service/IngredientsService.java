package com.recipes.server.service;

import java.util.List;

import com.recipes.server.models.Ingredient;

public interface IngredientsService {
	public Ingredient getIngredient(int id);

	public List<Ingredient> getIngredients();

	public void addIngredient(Ingredient ingredient);
	
	public void updateIngredient(Ingredient ingredient, int id);

	public void deleteIngredient(int id);
	
	public List<Ingredient> findByRecipeId(int id);

}
