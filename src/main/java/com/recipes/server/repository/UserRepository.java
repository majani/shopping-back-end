package com.recipes.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.recipes.server.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByUsername(String username);
}
