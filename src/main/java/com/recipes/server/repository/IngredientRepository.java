package com.recipes.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.recipes.server.models.Ingredient;

public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {

	List<Ingredient> findByRecipeId(int id);

	void deleteByRecipeId(int id);

	List<Ingredient> findByOrderByIdAsc();

}
