package com.recipes.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.recipes.server.models.User;
import com.recipes.server.service.UserService;
import com.recipes.server.util.AuthenticationRequest;
import com.recipes.server.util.AuthenticationResponse;
import com.recipes.server.util.JwtUtil;

@RestController
@CrossOrigin("http://localhost:4200")
public class RegistrationController {

	@Autowired
	private AuthenticationManager authManager;

	@Autowired
	UserDetailsService userDetSvc;
	
	@Autowired
	UserService userSvc;

	@Autowired
	JwtUtil jwtUtil;

	@Autowired
	BCryptPasswordEncoder bEncoder;

	@PostMapping("/register")
	public void registerUser(@RequestBody User user) {
		user.setPassword(bEncoder.encode(user.getPassword()));

		userSvc.addUser(user);
	}

	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {

		try {
			authManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
					authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect Username Password");

		}

		final UserDetails userDetails = userDetSvc.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtUtil.generateToken(userDetails);
		return new ResponseEntity(new AuthenticationResponse(jwt), HttpStatus.OK);
	}

}
