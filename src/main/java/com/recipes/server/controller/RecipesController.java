package com.recipes.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.recipes.server.models.Recipe;
import com.recipes.server.service.RecipesService;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/recipes")
public class RecipesController {

	@Autowired
	RecipesService recipeSvc;

	@GetMapping
	public List<Recipe> ListRecipes() {
		return recipeSvc.getRecipes();
	}

	@GetMapping("/{id}")
	public Recipe getRecipe(@PathVariable int id) {
		return recipeSvc.getRecipe(id);
	}

	@PostMapping
	public void addRecipe(@RequestBody Recipe recipe) {

		recipeSvc.addRecipe(recipe);
	}

	@PutMapping("/{id}")
	public void updateRecipe(@RequestBody Recipe recipe, @PathVariable int id) {
		recipeSvc.updateRecipe(recipe, id);
	}

	@PatchMapping("/{id}")
	public void patchRecipe(@RequestBody Recipe patch, @PathVariable int id) {
		Recipe recipe = recipeSvc.getRecipe(id);

		if (patch.getName() != null) {
			recipe.setName(patch.getName());
		}

		if (patch.getImagePath() != null) {
			recipe.setImagePath(patch.getImagePath());
		}

		if (patch.getDescription() != null) {
			recipe.setDescription(patch.getDescription());
		}

		if (patch.getIngredients() != null) {
			// recipe.setIngredients(null);
			recipe.setIngredients(patch.getIngredients());
		}
		
		recipeSvc.updateRecipe(recipe, id);

	}

	@DeleteMapping("/{id}")
	public void deleteRecipe(@PathVariable int id) {
		recipeSvc.deleteRecipe(id);
	}

}
