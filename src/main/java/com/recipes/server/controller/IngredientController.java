package com.recipes.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.recipes.server.models.Ingredient;
import com.recipes.server.repository.IngredientRepository;
import com.recipes.server.service.IngredientsService;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/ingredients")
public class IngredientController {

	@Autowired
	IngredientsService ingSvc;
	
	@Autowired
	IngredientRepository repo;

	@GetMapping
	public List<Ingredient> getIngredients() {
		return ingSvc.getIngredients();
	}

	@GetMapping("/{id}")
	public Ingredient getIngredient(@PathVariable int id) {
		return ingSvc.getIngredient(id);
	}

	@PostMapping
	public void addIngredient(@RequestBody Ingredient ingredient) {
		ingSvc.addIngredient(ingredient);
	}

	@PutMapping("/{id}")
	public void updateIngredient(@RequestBody Ingredient ingredient, @PathVariable int id) {
		this.ingSvc.updateIngredient(ingredient, id);
	}
	
	@PatchMapping("/{id}")
	public void updatePatch(@RequestBody Ingredient patch, @PathVariable int id) {
		Ingredient ing = repo.findById(id).get();
		
		if(patch.getAmount() != ing.getAmount()) {
			ing.setAmount(patch.getAmount());
		}
		
		if(patch.getName() != null) {
			ing.setName(patch.getName());
		}
		
		repo.save(ing);
	}

	@DeleteMapping("/{id}")
	public void deleteIngredient(@PathVariable int id) {
		ingSvc.deleteIngredient(id);
	}
}
